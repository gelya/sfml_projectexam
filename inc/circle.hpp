#pragma once
#include <SFML/Graphics.hpp>

namespace mt
{

	class Circle
	{
	public:
		Circle(int x, int y, float r)
		{
			m_x = x;
			m_y = y;
			m_r = r;
			m_circle->setOrigin(m_r, m_r);
			m_circle->setPosition(m_x, m_y);
			m_circle->setFillColor(sf::Color::Red);

		}
		~Circle()
		{
			delete m_circle;
		}

		sf::CircleShape* Get() { return m_circle; }

	private:
		int m_x, m_y;
		float m_r;
		sf::CircleShape* m_circle;

	};
}