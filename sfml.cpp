﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include "files/circle.hpp"


using namespace std::chrono_literals;

int main()
{


    // Создание окна с известными размерами и названием
    sf::RenderWindow window(sf::VideoMode(800, 600), "Progect Zhdanova");

    mt::Circle circle(0, 600, 200);

    // Цикл работает до тех пор, пока окно открыто
    while (window.isOpen())
    {
        // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            // Обработка событий
            // Если нажат крестик, то
            if (event.type == sf::Event::Closed)
                // окно закрывается
                window.close();
        }

        // Движение шарика



        // Очистить окно от всего
        window.clear();


        // Перемещение фигуры в буфер

        window.draw(*circle.Get());

        // Отобразить на окне все, что есть в буфере
        window.display();


    }

    return 0;
}
